import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/database_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    String? email = sharedPreferences.getString("email");
    String? password = sharedPreferences.getString("password");
    var db = new DatabaseHelper();
    User? userRetorno = new User(email: null, password: null, userName: null);
    userRetorno = await db
        .selectUser(User(email: email, password: password, userName: ""));
    if (isLoggedIn == true && userRetorno != null) {
      emit(AuthBlocLoggedInState());
    } else {
      emit(AuthBlocLoginState());
    }
  }

  void loginUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    var db = new DatabaseHelper();
    User? userRetorno = new User(email: null, password: null, userName: null);
    userRetorno = await db.selectUser(user);
    if (userRetorno != null) {
      await sharedPreferences.setBool("is_logged_in", true);
      await sharedPreferences.setString("email", user.email!);
      await sharedPreferences.setString("password", user.password!);
      await sharedPreferences.setString("username", user.userName!);
      emit(AuthBlocLoggedInState());
    } else {
      emit(AuthBlocErrorState("Login Gagal!"));
    }
  }

  void registerUser(User user) async {
    emit(AuthBlocLoadingState());
    var db = new DatabaseHelper();
    await db.saveUser(user);
    emit(AuthBlocRegisterInState());
  }

  void logoutUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    await sharedPreferences.remove("is_logged_in");
    await sharedPreferences.remove("email");
    await sharedPreferences.remove("password");
    await sharedPreferences.remove("username");
    emit(AuthBlocLoggedOutState());
  }
}
