// Validation
// Validation for the input field
class Validation {
  // Valid if input is required
  String? validateNotNull(String? value, fieldName) {
    if (value == "") {
      return 'Field $fieldName is required';
    } else {
      return null;
    }
  }

  // Valid if input is email format
  String? validateEmail(String email) {
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
    if (email != "") {
      return pattern.hasMatch(email) ? null : 'Your Email is invalid';
    } else {
      validateNotNull(email, "Email");
    }
  }

  // Valid if input is username format
  String? validateUsername(String name) {
    Pattern pattern = r'^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$';
    RegExp regex = new RegExp(pattern as String);
    if (!regex.hasMatch(name))
      return 'Username not valid';
    else
      return null;
  }

  // Valid if input is password format
  String? validatePassword(String password) {
    Pattern number = r'^(?=.*?[0-9]+.*)';
    Pattern alphabet = r'(?=.*?[a-zA-Z]+.*)';
    // Pattern character = r'(?=.*?[!@#\$&*~]+.*)';
    RegExp regex = new RegExp(number as String);
    if (password.isEmpty) {
      return 'Field Password is required';
    } else if (password.length < 8) {
      return 'Password is to short';
    } else {
      if (!regex.hasMatch(password)) {
        return 'Password must be contain numbers';
      } else {
        regex = new RegExp(alphabet as String);
        if (!regex.hasMatch(password)) {
          return 'Password must contain alphabets';
        }
        return null;
      }
    }
  }
}
