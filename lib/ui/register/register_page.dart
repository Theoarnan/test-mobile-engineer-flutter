// ignore_for_file: unnecessary_statements

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/app_bar.dart';
import 'package:majootestcase/utils/validations.dart';
import 'package:sizer/sizer.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  final _passwordConfirmController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, title: "Register"),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Register',
                style: TextStyle(
                  fontSize: 32.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
              Text(
                'Please register your account',
                style: TextStyle(
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 5.h,
              ),
              _form(),
              SizedBox(
                height: 5.h,
              ),
              Center(
                child: ElevatedButton(
                  onPressed: () => handleRegister(),
                  child: Text("SUBMIT"),
                  style: ElevatedButton.styleFrom(
                    padding:
                        EdgeInsets.symmetric(horizontal: 30.w, vertical: 1.8.h),
                  ),
                ),
              ),
            ],
          ),
        ),
      )),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'username',
            label: 'Username',
            validator: (value) =>
                Validation().validateNotNull(value, "Username"),
          ),
          SizedBox(
            height: 14,
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'example@123.com',
            label: 'Email',
            validator: (value) => Validation().validateEmail(value!),
          ),
          SizedBox(
            height: 14,
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
            validator: (value) => Validation().validatePassword(value!),
          ),
          SizedBox(
            height: 14,
          ),
          CustomTextFormField(
            context: context,
            label: 'Confirm Password',
            hint: 'confirm password',
            controller: _passwordConfirmController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
            validator: (value) {
              if (value != _passwordController.value) {
                return "Password Not Match";
              } else {
                return null;
              }
            },
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final String? _username = _usernameController.value;
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    if (formKey.currentState!.validate()) {
      User user = User(email: _email, password: _password, userName: _username);
      BlocProvider.of<AuthBlocCubit>(context).registerUser(user);
      _emailController.value = "";
      _passwordController.value = "";
      _usernameController.value = "";
      _passwordConfirmController.value = "";
    }
  }
}
