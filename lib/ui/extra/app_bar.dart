import 'package:flutter/material.dart';

AppBar buildAppBar(BuildContext context,
    {required String title, bool withBackButton = true}) {
  return AppBar(
    title: Text(title),
    leading: withBackButton
        ? IconButton(
            splashRadius: 3,
            icon: Icon(
              Icons.arrow_back_sharp,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pop(
              context,
            ),
          )
        : null,
  );
}
