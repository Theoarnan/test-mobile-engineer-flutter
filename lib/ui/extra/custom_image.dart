import 'package:flutter/material.dart';

class CustomNetworkImage extends StatelessWidget {
  final String image;
  final double width;
  final double height;
  final BoxFit? fit;
  final String? imageErrorUrl;
  final bool isImageErrorSvg;

  const CustomNetworkImage({
    Key? key,
    required this.image,
    required this.width,
    required this.height,
    this.fit = BoxFit.fill,
    this.imageErrorUrl,
    this.isImageErrorSvg = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      image,
      width: width,
      fit: fit,
      errorBuilder: (_, __, ___) {
        if (imageErrorUrl != null) {
          Image.network(
            imageErrorUrl!,
            width: width,
          );
        }
        return Image.asset(
          "assets/no-image-sm.png",
          fit: fit,
          height: height,
        );
      },
      loadingBuilder: (_, Widget child, ImageChunkEvent? loadingProgress) {
        if (loadingProgress == null) return child;
        return Container(
          width: width,
          child: Center(
            child: CircularProgressIndicator(
              value: loadingProgress.expectedTotalBytes != null
                  ? loadingProgress.cumulativeBytesLoaded /
                      loadingProgress.expectedTotalBytes!
                  : null,
            ),
          ),
        );
      },
    );
  }
}
