import 'package:flutter/material.dart';
import 'package:majootestcase/utils/connection.dart';
import 'package:open_settings/open_settings.dart';
import 'package:provider/provider.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:sizer/sizer.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Connection Error",
              style:
                  TextStyle(fontSize: 14.sp, color: textColor ?? Colors.black),
            ),
            retry != null
                ? Column(
                    children: [
                      SizedBox(
                        height: 100,
                      ),
                      retryButton ??
                          IconButton(
                            onPressed: retry,
                            icon: Icon(Icons.refresh_sharp),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}

class NetworkAwareWidget extends StatelessWidget {
  final Widget onlineChild;
  final Widget offlineChild;

  const NetworkAwareWidget(
      {Key? key, required this.onlineChild, required this.offlineChild})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    NetworkStatus networkStatus = Provider.of<NetworkStatus>(context);
    if (networkStatus == NetworkStatus.Online) {
      return onlineChild;
    } else {
      _showToastMessage(context: context, message: "Offline");
      return offlineChild;
    }
  }

  void _showToastMessage(
      {required String message, required BuildContext context}) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Colors.red,
      duration: Duration(seconds: 3),
      icon: Icon(Icons.dangerous),
      title: "No Internet",
      message: "Your is " + message,
    );
  }
}

class NoInternetPage extends StatelessWidget {
  const NoInternetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Ooopss!",
              style: TextStyle(fontWeight: FontWeight.w800, fontSize: 24.sp),
            ),
            Container(
              height: 30.h,
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/ilus_no_internet.png"),
                    fit: BoxFit.contain),
              ),
            ),
            Text(
              "No Connection",
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14.sp),
            ),
            SizedBox(
              height: 1.h,
            ),
            Text(
              "Please check your connectivity.",
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14.sp),
            ),
            SizedBox(
              height: 3.h,
            ),
            Container(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 4),
                  primary: Colors.blue,
                ),
                onPressed: () {
                  OpenSettings.openMainSetting();
                },
                child: Text("Open Setting"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
