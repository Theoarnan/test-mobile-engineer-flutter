import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/extra/app_bar.dart';
import 'package:majootestcase/ui/extra/custom_image.dart';
import 'package:sizer/sizer.dart';

class DetailMoviePage extends StatefulWidget {
  final Data data;
  const DetailMoviePage({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  _DetailMoviePageState createState() => _DetailMoviePageState();
}

class _DetailMoviePageState extends State<DetailMoviePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, title: "Details Movie"),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // YEAR IMAGE
              CustomNetworkImage(
                height: 0,
                width: 100.w,
                image: widget.data.i!.imageUrl!,
                fit: BoxFit.cover,
              ),
              SizedBox(
                height: 6,
              ),
              Padding(
                padding: EdgeInsets.all(25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    // YEAR MOVIE
                    SizedBox(
                      child: Text(
                        widget.data.year.toString(),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textDirection: TextDirection.ltr,
                        style: TextStyle(
                            fontSize: 12.sp,
                            color: Colors.pink,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    // TITLE MOVIE
                    SizedBox(
                      child: Text(
                        widget.data.l!,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textDirection: TextDirection.ltr,
                        style: TextStyle(
                            fontSize: 14.sp, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    // SERI MOVIE
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 8.w, vertical: 0.5.h),
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(
                                12.0) //                 <--- border radius here
                            ),
                      ),
                      child: SizedBox(
                        child: Text(
                          widget.data.q!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                              fontSize: 10.sp,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    // SERIES MOVIE
                    widget.data.series == null
                        ? SizedBox()
                        : Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 2.w, vertical: 1.h),
                            height: 10.h,
                            child: ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: widget.data.series!.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      // IMAGE SERIES
                                      CustomNetworkImage(
                                        width: 20.w,
                                        height: 0,
                                        image: widget
                                            .data.series![index].i!.imageUrl!,
                                        fit: BoxFit.cover,
                                      ),
                                      SizedBox(
                                        height: 2.w,
                                      ),
                                      // TITLE SERIES
                                      SizedBox(
                                        width: 110,
                                        child: Text(
                                          widget.data.series![index].l!,
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 9.sp,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          )
                  ],
                ),
              )
            ],
          ),
        ),
      )),
    );
  }
}
