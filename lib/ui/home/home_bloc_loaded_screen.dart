import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/extra/app_bar.dart';
import 'package:majootestcase/ui/extra/custom_image.dart';
import 'package:majootestcase/ui/home/detail_movie_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';

class HomeBlocLoadedScreen extends StatefulWidget {
  final int bottomNavBarIndex;
  final List<Data>? data;

  const HomeBlocLoadedScreen({
    Key? key,
    required this.bottomNavBarIndex,
    this.data,
  }) : super(key: key);

  @override
  State<HomeBlocLoadedScreen> createState() => _HomeBlocLoadedScreenState();
}

class _HomeBlocLoadedScreenState extends State<HomeBlocLoadedScreen> {
  int? bottomNavBarIndex;
  PageController? pageController;

  @override
  void initState() {
    super.initState();
    bottomNavBarIndex = widget.bottomNavBarIndex;
    pageController = PageController(initialPage: bottomNavBarIndex!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, title: "Home", withBackButton: false),
      body: PageView(
        controller: pageController,
        physics: BouncingScrollPhysics(),
        onPageChanged: (index) {
          setState(() {
            bottomNavBarIndex = index;
          });
        },
        children: [
          // HOME PAGE
          _buildHomePage(context),
          // PROFILE PAGE
          _buildProfilePage(context)
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: bottomNavBarIndex!,
          elevation: 0,
          backgroundColor: Colors.white,
          selectedItemColor: Colors.blue,
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              bottomNavBarIndex = index;
              pageController!.jumpToPage(index);
            });
          },
          items: [
            _bottomNavBarItem(title: "Home", icon: Icon(Icons.home)),
            _bottomNavBarItem(title: "Profile", icon: Icon(Icons.person))
          ]),
    );
  }

  SafeArea _buildProfilePage(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () =>
                  BlocProvider.of<AuthBlocCubit>(context).logoutUser(),
              child: Text("LOGOUT"),
              style: ElevatedButton.styleFrom(
                padding:
                    EdgeInsets.symmetric(horizontal: 30.w, vertical: 1.8.h),
              ),
            ),
          ],
        ),
      ),
    );
  }

  SafeArea _buildHomePage(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Container(
          height: 84.h,
          child: RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<HomeBlocCubit>(context)..fetchingData();
            },
            child: ListView.builder(
              itemCount: widget.data!.length,
              itemBuilder: (context, index) {
                return _movieItemWidget(widget.data![index]);
              },
            ),
          ),
        ),
      ),
    );
  }

  // CARD Movie ITEM
  Widget _movieItemWidget(Data data) {
    return InkWell(
      onTap: () async {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => DetailMoviePage(
              data: data,
            ),
          ),
        );
      },
      child: Card(
        elevation: 1,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.all(2.h),
                child: CustomNetworkImage(
                  height: 25.h,
                  width: 25.w,
                  image: data.i!.imageUrl!,
                  fit: BoxFit.cover,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    child: Text(
                      data.year.toString(),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          fontSize: 11.sp,
                          color: Colors.pink,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  SizedBox(
                    width: 60.w,
                    child: Text(
                      data.l!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          fontSize: 13.sp, fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.w, vertical: 0.5.h),
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(
                              12.0) //                 <--- border radius here
                          ),
                    ),
                    child: SizedBox(
                      child: Text(
                        data.q!,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textDirection: TextDirection.ltr,
                        style: TextStyle(
                            fontSize: 10.sp,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.star,
                        color: Colors.yellow,
                      ),
                      SizedBox(
                        width: 1.w,
                      ),
                      SizedBox(
                        child: Text(
                          data.rank.toString(),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                              fontSize: 11.sp, fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  //* ITEM BOTTOM NAV BAR
  BottomNavigationBarItem _bottomNavBarItem(
      {required String title, required Icon icon}) {
    return BottomNavigationBarItem(
      title: Text(title,
          style: TextStyle(fontSize: 10, fontWeight: FontWeight.w600)),
      icon: Container(
        margin: EdgeInsets.only(bottom: 6),
        height: 24,
        child: icon,
      ),
    );
  }
}
