import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'form_field.dart';

class CustomTextFormField extends CustomFormField<String?> {
  CustomTextFormField({
    String? label,
    String? title,
    String? hint,
    TextController? controller,
    BuildContext? context,
    bool enabled = true,
    bool mandatory = true,
    bool isObscureText = false,
    bool isEmail = false,
    bool isDigit = false,
    FormFieldValidator<String>? validator,
    double padding = 4,
    TextInputAction? textInputAction,
    Widget? suffixIcon,
    Key? key,
  }) : super(
          key: key,
          controller: controller,
          enabled: enabled,
          builder: (FormFieldState<String?> state) {
            return _CustomTextForm(
              label: label,
              controller: controller,
              hint: hint,
              state: state,
              mandatory: mandatory,
              isObscureText: isObscureText,
              suffixIcon: suffixIcon,
              isEmail: isEmail,
              isDigit: isDigit,
              padding: padding,
              textInputAction: textInputAction,
            );
          },
          validator: (picker) {
            if (mandatory && (picker == null || picker.isEmpty)) {
              return 'this field is required';
            }
            if (validator != null) {
              return validator(picker);
            }
            return null;
          },
        );
}

class TextController extends CustomFormFieldController<String?> {
  TextController({String? initialValue}) : super(initialValue);

  @override
  String? fromValue(String? value) => value;

  @override
  String toValue(String text) => text;
}

class _CustomTextForm extends StatefulWidget {
  final FormFieldState<String?>? state;
  final TextController? controller;
  final double padding;
  final String? label;
  final String? hint;
  final bool mandatory;
  final bool isObscureText;
  final bool isEmail;
  final bool isDigit;
  final TextInputAction? textInputAction;
  final Widget? suffixIcon;

  const _CustomTextForm({
    this.state,
    this.controller,
    this.padding = 0,
    this.label,
    this.hint,
    this.mandatory = false,
    this.isObscureText = false,
    this.isEmail = false,
    this.isDigit = false,
    this.textInputAction,
    this.suffixIcon,
  });

  @override
  State<StatefulWidget> createState() => _CustomTextFormState();
}

class _CustomTextFormState extends State<_CustomTextForm> {
  final _focusNode = FocusNode();

  String? get _hint => widget.hint;

  bool get _mandatory => widget.mandatory;

  String? get _label {
    var fullLabel = StringBuffer();
    final label = widget.label;
    if (label != null) {
      fullLabel.write(label);
      if (_mandatory) fullLabel.write(' *');
      return fullLabel.toString();
    }
    return label;
  }

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    final inputFormatters = <TextInputFormatter>[];
    TextInputType? keyboardType;

    if (widget.isDigit) {
      inputFormatters.add(FilteringTextInputFormatter.digitsOnly);
      keyboardType = TextInputType.number;
    }
    if (widget.isEmail) {
      keyboardType = TextInputType.emailAddress;
    }
    final _hint = this._hint;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (_label != null)
          Text(
            _label!,
          )
        else
          Container(),
        SizedBox(
          height: 8,
        ),
        TextFormField(
          autofocus: true,
          obscureText: widget.isObscureText,
          textInputAction: widget.textInputAction,
          keyboardType: keyboardType,
          inputFormatters: inputFormatters,
          focusNode: _focusNode,
          controller: widget.controller?.textController,
          decoration: defaultInputDecoration.copyWith(
            hintText: _hint != null ? _hint : 'Fill the field here',
            suffixIcon: _focusNode.hasFocus
                ? widget.suffixIcon ??
                    IconButton(
                      icon: const Icon(Icons.cancel),
                      onPressed: () {
                        widget.controller?.textController.clear();
                      },
                    )
                : null,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            errorText: widget.state!.errorText,
            isDense: _label == null,
            errorStyle: TextStyle(),
            fillColor: Colors.white,
            border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Colors.blue, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(12),
            ),
            errorBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Colors.red, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(12),
            ),
            contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
          ),
          //* Styling Text Field
        ),
      ],
    );
  }
}
