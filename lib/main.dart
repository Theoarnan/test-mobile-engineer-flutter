import 'package:another_flushbar/flushbar.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/home/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/utils/connection.dart';
import 'package:sizer/sizer.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(BlocProvider(
    create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
    child: DemoApp(),
  ));
}

class DemoApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      EasyLoading.instance
        ..maskType = EasyLoadingMaskType.black
        ..loadingStyle = EasyLoadingStyle.custom
        ..backgroundColor = Colors.white
        ..textColor = Colors.black
        ..indicatorColor = Colors.blue;
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        builder: EasyLoading.init(),
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: BuilderState(),
      );
    });
  }
}

class BuilderState extends StatelessWidget {
  const BuilderState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBlocCubit, AuthBlocState>(
      listener: (context, state) {
        if (state is AuthBlocLoggedInState) {
          EasyLoading.dismiss();
          Flushbar(
            flushbarPosition: FlushbarPosition.BOTTOM,
            backgroundColor: Colors.green,
            duration: Duration(seconds: 5),
            message: "Login Berhasil",
          )..show(context);
        }
        if (state is AuthBlocErrorState) {
          EasyLoading.dismiss();
          Flushbar(
            flushbarPosition: FlushbarPosition.TOP,
            backgroundColor: Colors.red,
            duration: Duration(seconds: 5),
            message: state.error,
          )..show(context);
        }
        if (state is AuthBlocRegisterInState) {
          EasyLoading.dismiss();
          Flushbar(
            flushbarPosition: FlushbarPosition.BOTTOM,
            backgroundColor: Colors.green,
            duration: Duration(seconds: 5),
            message: "Register Berhasil",
          )..show(context);
        }
        if (state is AuthBlocLoadingState) {
          EasyLoading.show(dismissOnTap: false);
        }
      },
      builder: (context, state) {
        if (state is AuthBlocLoginState || state is AuthBlocLoggedOutState) {
          EasyLoading.dismiss();
          return LoginPage();
        }
        if (state is AuthBlocLoggedInState) {
          return StreamProvider<NetworkStatus>(
            initialData: NetworkStatus.Online,
            lazy: true,
            create: (context) =>
                NetworkStatusService().networkStatusController.stream,
            child: NetworkAwareWidget(
              offlineChild: NoInternetPage(),
              onlineChild: BlocProvider(
                create: (context) => HomeBlocCubit()..fetchingData(),
                child: HomeBlocScreen(),
              ),
            ),
          );
        }
        return LoginPage();
      },
    );
  }
}
